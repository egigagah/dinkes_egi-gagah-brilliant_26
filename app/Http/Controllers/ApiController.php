<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ApiController extends Controller
{
    public function home(Request $request)
    {
        $source1 = Http::withHeaders([
            'Authorization' => 'rahasia',
        ])->get('https://dinkes.jakarta.go.id/api_rekrutmen/puskesmas');

        $source2 = Http::withHeaders([
            'Authorization' => 'rahasia',
        ])->get('https://dinkes.jakarta.go.id/api_rekrutmen/kecamatan');

        $puskesmas = $source1->json()['data'];
        $kecamatan = $source2->json()['data'];

        $data = collect($puskesmas)->map(function ($item, $key) use($kecamatan) {
            $wilayah = [];
            $provinsi = [];
            $kota = [];
            $kecArr = [];

            foreach ($kecamatan as $data){ 
                if($item['kode_kecamatan'] == $data['id_kecamatan']) {
                    $wilayah = $data;
                }
            }

            if(count($wilayah) > 0) {
                $provinsi = [
                    'id_provinsi' => $wilayah['id_provinsi'], 
                    'nama_provinsi' => $wilayah['nama_provinsi']
                ];
                $kota = [
                    'id_kota' => $wilayah['id_kota'], 
                    'nama_kota' => $wilayah['nama_kota']
                ];
                $kecArr = [
                    'id_kecamatan' => $wilayah['id_kecamatan'], 
                    'nama_kecamatan' => $wilayah['nama_kecamatan']
                ];
            }

            $item['provinsi'] = $provinsi;
            $item['kota'] = $kota;
            $item['kecamatan'] = $kecArr;
            unset($item['kode_kecamatan']);
            return $item;
        });

        return response()->json($data);
    }
}
